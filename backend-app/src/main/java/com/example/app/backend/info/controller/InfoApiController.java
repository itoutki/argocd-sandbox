package com.example.app.backend.info.controller;


import com.example.app.backend.info.config.AppInfoConfig;
import com.example.app.backend.info.model.AppInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

@RestController
public class InfoApiController {

    AppInfoConfig appInfoConfig;

    public InfoApiController(AppInfoConfig appInfoConfig) {
        this.appInfoConfig = appInfoConfig;
    }

    @GetMapping("/api/info")
    public AppInfo info() {
        AppInfo appInfo = AppInfo.fromAppInfoConfig(appInfoConfig);
        return appInfo;
    }
}
