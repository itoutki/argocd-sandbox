package com.example.app.frontend.info.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "app.info")
public class AppInfoConfig {
    String version;
    String appname;
    String podname;
    String namespace;
    String nodename;
    String env;
    String backendurl;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getPodname() {
        return podname;
    }

    public void setPodname(String podname) {
        this.podname = podname;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getNodename() {
        return nodename;
    }

    public void setNodename(String nodename) {
        this.nodename = nodename;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getBackendurl() {
        return backendurl;
    }

    public void setBackendurl(String backendurl) {
        this.backendurl = backendurl;
    }
}
