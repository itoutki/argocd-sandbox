package com.example.app.frontend.info.controller;

import com.example.app.frontend.info.config.AppInfoConfig;
import com.example.app.frontend.info.model.AppInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

@RestController
public class InfoApiController {

    AppInfoConfig appInfoConfig;
    WebClient webClient;

    public InfoApiController(AppInfoConfig appInfoConfig, WebClient.Builder builder) {
        this.appInfoConfig = appInfoConfig;
        this.webClient = builder.baseUrl(appInfoConfig.getBackendurl()).build();
    }

    @GetMapping("/api/info")
    public AppInfo info() {
        AppInfo appInfo = AppInfo.fromAppInfoConfig(appInfoConfig);
        AppInfo backendAppInfo = webClient.get()
                .uri("/api/info")
                .retrieve()
                .bodyToMono(AppInfo.class)
                .block();
        appInfo.setBackendAppInfo(backendAppInfo);
        return appInfo;
    }

    @GetMapping("/api/info/self")
    public AppInfo selfInfo() {
        AppInfo appInfo = AppInfo.fromAppInfoConfig(appInfoConfig);
        return appInfo;
    }

    @GetMapping("/api/info/backend")
    public AppInfo backendInfo() {
        AppInfo appInfo = webClient.get()
                .uri("/api/info")
                .retrieve()
                .bodyToMono(AppInfo.class)
                .block();
        return appInfo;
    }

}
