package com.example.app.frontend.info.model;

import com.example.app.frontend.info.config.AppInfoConfig;

public class AppInfo {

    String version;
    String appName;
    String podName;
    String namespace;
    String nodeName;
    String env;
    String backendUrl;
    AppInfo backendAppInfo;

    public AppInfo(String version,
                   String appName,
                   String podName,
                   String namespace,
                   String nodeName,
                   String env,
                   String backendUrl,
                   AppInfo backendAppInfo) {
        this.version = version;
        this.appName = appName;
        this.podName = podName;
        this.namespace = namespace;
        this.nodeName = nodeName;
        this.env = env;
        this.backendUrl = backendUrl;
        this.backendAppInfo = backendAppInfo;
    }

    public static AppInfo fromAppInfoConfig(AppInfoConfig appInfoConfig) {
        AppInfo appInfo = new AppInfo(appInfoConfig.getVersion(),
                appInfoConfig.getAppname(),
                appInfoConfig.getPodname(),
                appInfoConfig.getNamespace(),
                appInfoConfig.getNodename(),
                appInfoConfig.getEnv(),
                appInfoConfig.getBackendurl(),
                null);
        return appInfo;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPodName() {
        return podName;
    }

    public void setPodName(String podName) {
        this.podName = podName;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getBackendUrl() {
        return backendUrl;
    }

    public void setBackendUrl(String backendUrl) {
        this.backendUrl = backendUrl;
    }

    public AppInfo getBackendAppInfo() {
        return backendAppInfo;
    }

    public void setBackendAppInfo(AppInfo backendAppInfo) {
        this.backendAppInfo = backendAppInfo;
    }
}
